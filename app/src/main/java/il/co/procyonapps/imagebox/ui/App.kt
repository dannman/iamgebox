package il.co.procyonapps.imagebox.ui

import android.app.Application
import il.co.procyonapps.imagebox.R
import il.co.procyonapps.imagebox.network.PixabayApi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class App : Application() {

    private val retrofit by lazy {
        val client = OkHttpClient.Builder()
            .apply {
                addInterceptor { chain ->
                    val origRequest = chain.request()
                    val origUrl = origRequest.url()

                    val newUrl = origUrl.newBuilder()
                        .addQueryParameter("key", getString(R.string.api_key))
                        .build()

                    val newRequest = origRequest.newBuilder()
                        .url(newUrl)
                        .build()

                    chain.proceed(newRequest)

                }
            }.build()


        Retrofit.Builder()
            .baseUrl("https://pixabay.com")
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    val api by lazy { retrofit.create(PixabayApi::class.java) }

}