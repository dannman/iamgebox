package il.co.procyonapps.imagebox

import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment

fun Fragment.hideKeyboard() {
    this.view?.rootView?.windowToken?.let {
        (context?.getSystemService(Activity.INPUT_METHOD_SERVICE) as? InputMethodManager)
            ?.hideSoftInputFromWindow(it, 0)
    }
}

var View.gone: Boolean
    set(value) {
        visibility = if (value) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }
    get() {
        return visibility == View.GONE
    }