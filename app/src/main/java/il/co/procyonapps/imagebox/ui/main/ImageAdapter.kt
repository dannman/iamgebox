package il.co.procyonapps.imagebox.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isGone
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import il.co.procyonapps.imagebox.R
import il.co.procyonapps.imagebox.data.Image
import il.co.procyonapps.imagebox.gone
import kotlinx.android.synthetic.main.image_item.view.*

class ImageAdapter(val onItemClick: (Int) -> Unit) :
    ListAdapter<Image, ImageAdapter.ImageViewHolder>(ImageDiffer()) {
    private val picasso = Picasso.get()

    class ImageDiffer : DiffUtil.ItemCallback<Image>() {
        override fun areItemsTheSame(oldItem: Image, newItem: Image): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Image, newItem: Image): Boolean {
            return oldItem.id == newItem.id &&
                    oldItem.previewUrl == newItem.previewUrl &&
                    oldItem.fullsizeUrl == newItem.fullsizeUrl &&
                    oldItem.columns == newItem.columns &&
                    oldItem.imageRatio == newItem.imageRatio
        }
    }



    inner class ImageViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(image: Image) {
            picasso.load(image.fullsizeUrl)
                .error(R.drawable.ic_broken_image_black_24dp)
                .fit()
                .stableKey("${image.id}")
                .into(view.ivImage)


            view.setOnClickListener { onItemClick(image.id) }

            view.tvLable.text = "columns: ${image.columns}"
            view.invalidate()

        }
    }

    val spanSizeLookup: GridLayoutManager.SpanSizeLookup by lazy {
        object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return getItem(position).columns
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        return LayoutInflater.from(parent.context).inflate(R.layout.image_item, parent, false).let {
            ImageViewHolder(it)
        }
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}