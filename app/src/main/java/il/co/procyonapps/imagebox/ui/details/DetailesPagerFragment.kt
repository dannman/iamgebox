package il.co.procyonapps.imagebox.ui.details

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import il.co.procyonapps.imagebox.MainActivity
import il.co.procyonapps.imagebox.R
import il.co.procyonapps.imagebox.data.Image
import il.co.procyonapps.imagebox.ui.main.MainViewModel
import kotlinx.android.synthetic.main.details_pager_fragment.*

class DetailesPagerFragment : Fragment() {


    val args: DetailesPagerFragmentArgs by navArgs()


    private val viewModel: MainViewModel by lazy {
        (activity as MainActivity).viewModel
    }

    private val pagerAdapter by lazy { DetailesAdapter(this) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.details_pager_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("DetailesPagerFragment", "created. selected image id: ${args.currentImageId}")

        viewModel.searchResults.observe(viewLifecycleOwner, Observer { imageList ->
            pagerAdapter.images = imageList

            //find index of current element
            var element: Image? = null
            for (it in imageList) {
                if (it.id == args.currentImageId) {
                    element = it
                    break
                }
            }
            element?.let {
                pager.setCurrentItem(imageList.indexOf(it), false)
            }

        })

        pager.apply {
            adapter = pagerAdapter
            offscreenPageLimit = 2
        }
    }

}