package il.co.procyonapps.imagebox.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Image(val previewUrl: String, val fullsizeUrl: String, val likes: Int, val favorites: Int, val id: Int, val width: Int, val height: Int, var columns: Int = 0, var isLast: Boolean = false): Parcelable {

    val imageRatio: Float = width.toFloat() / height.toFloat()
}