package il.co.procyonapps.imagebox.ui.details

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import il.co.procyonapps.imagebox.data.Image

class DetailesAdapter(frag: Fragment) : FragmentStateAdapter(frag) {

    var images: List<Image> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int {
        return images.size
    }

    override fun createFragment(position: Int): Fragment {
        val image = images[position]
        return DetailsFragment.newInstance(image)
    }
}