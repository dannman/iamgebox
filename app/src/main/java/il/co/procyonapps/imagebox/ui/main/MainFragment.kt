package il.co.procyonapps.imagebox.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.Toast
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import il.co.procyonapps.imagebox.MainActivity
import il.co.procyonapps.imagebox.R
import il.co.procyonapps.imagebox.data.Image
import il.co.procyonapps.imagebox.gone
import il.co.procyonapps.imagebox.hideKeyboard
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment() {


    private val imageAdapter by lazy { ImageAdapter(onImageClick) }
    var searchView: SearchView? = null

    private val viewModel: MainViewModel by lazy {
        (activity as MainActivity).viewModel
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //setup toolbar:
        toolbar.apply {
            inflateMenu(R.menu.main_screen_menu)

            searchView = (menu.findItem(R.id.app_bar_search).actionView as SearchView?)?.let {
                it.queryHint = "Search Images"
//                it.isIconified = false
                it.setOnQueryTextListener(searchListener)
                it

            }
        }

        rvImageList.apply {
            adapter = imageAdapter
            val gridLayoutManager = GridLayoutManager(context, 15).also {
                it.spanSizeLookup = imageAdapter.spanSizeLookup
            }
            layoutManager = gridLayoutManager
            addOnScrollListener(scrollListener)
        }

        viewModel.searchResults.observe(viewLifecycleOwner, Observer<List<Image>> { imageList ->
            imageAdapter.submitList(imageList)
            tvTip.gone = imageList.isNotEmpty()
        })

        viewModel.statusUpdater.observe(viewLifecycleOwner, Observer<String>{
            Toast.makeText(context, "$it", Toast.LENGTH_SHORT).show()
        })


    }

   private val scrollListener = object: RecyclerView.OnScrollListener(){
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            if(!rvImageList.canScrollVertically(1)){
                //try to load next page:
                viewModel.loadNextPage()
            }
        }
    }

    private val onImageClick: (Int) -> Unit = {

        val action = MainFragmentDirections.actionMainFragmentToImageDetailes(it)
        findNavController().navigate(action)
    }

    private val searchListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            query?.let {
                viewModel.queryImages(it)
            }
            hideKeyboard()
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            return false
        }
    }
}
