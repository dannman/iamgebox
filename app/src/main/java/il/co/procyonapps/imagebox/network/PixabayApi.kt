package il.co.procyonapps.imagebox.network

import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PixabayApi {

    @GET("/api/")
    suspend fun query(
        @Query("q") query: String,
        @Query("page") page: Int,
        @Query("image_type") type: String = "photo"
    ): QueryResponse
}