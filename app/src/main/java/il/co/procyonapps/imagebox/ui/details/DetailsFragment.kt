package il.co.procyonapps.imagebox.ui.details

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.squareup.picasso.Picasso
import il.co.procyonapps.imagebox.R
import il.co.procyonapps.imagebox.data.Image
import kotlinx.android.synthetic.main.details_fragment.*

class DetailsFragment : Fragment() {

    companion object {
        const val IMAGE = "IMAGE"

        fun newInstance(image: Image): DetailsFragment {
            return DetailsFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(IMAGE, image)
                }
            }
        }
    }

    val image by lazy { requireArguments().getParcelable<Image>(IMAGE) as Image }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.details_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("DetailsFragment", "created. image url: $image")
        Picasso.get().load(image.fullsizeUrl)
            .into(ivImage)

        tvLikes.text = "${image.likes}"
        tvFavorites.text = "${image.favorites}"
    }
}