package il.co.procyonapps.imagebox.ui.main

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import il.co.procyonapps.imagebox.data.Image
import il.co.procyonapps.imagebox.network.QueryResponse
import il.co.procyonapps.imagebox.ui.App
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import java.lang.Exception

class MainViewModel(app: Application) : AndroidViewModel(app) {

   private var pageIndex = 2
   private var lastId: Int = 0
   private var lastQuery: String? = null

    private val api by lazy { (app as App).api }

    val searchResults: MutableLiveData<List<Image>> by lazy { MutableLiveData<List<Image>>() }

    val statusUpdater: MutableLiveData<String> by lazy { MutableLiveData<String>() }


    fun queryImages(query: String) = MainScope().launch {
        Log.d("MainViewModel", "query: $query")
        lastQuery = query
        pageIndex = 2
        try {
            val response = api.query(query, 1)
            Log.d("MainViewModel", "response: $response")
            lastId = response.hits.last().id

            val parsed = response.hits.mapToImage()
            searchResults.postValue(parsed)
        } catch (e: Exception) {
            Log.e("MainViewModel", e.message)
        }

    }


    fun loadNextPage() = MainScope().launch {
        if (lastQuery == null || searchResults.value?.isNullOrEmpty() == true) {
            statusUpdater.postValue("no previous pages detected!")
        } else {
            val nextPage = api.query(lastQuery!!, pageIndex)
            if (nextPage.hits.isNotEmpty() && nextPage.hits.last().id != lastId) {
                statusUpdater.postValue("new page loaded: $pageIndex")
                //we have a new page!!
                pageIndex++
                lastId = nextPage.hits.last().id

                val newList = searchResults.value as List<Image> + nextPage.hits.mapToImage()
                searchResults.postValue(newList)


            } else {
                statusUpdater.postValue("no more pages to load")
            }
        }
    }

    private fun List<QueryResponse.Hit>.mapToImage(columns: Int = 15): List<Image> {

        val list = ArrayList<Image>()
        val row = ArrayList<Image>()
        var rowRatios = 0f

        for (hit in this) {
            val url = if (!hit.webformatURL.isNullOrEmpty()) {
                hit.webformatURL
            } else if (!hit.userImageURL.isNullOrEmpty()) {
                hit.userImageURL
            } else {
                "null"
            }
            val item = Image(
                hit.previewURL,
                url,
                hit.likes,
                hit.favorites,
                hit.id,
                hit.imageWidth,
                hit.imageHeight
            )

            list.add(item)
            rowRatios += item.imageRatio

            if (rowRatios > 2) {
                var used = 0
                row.forEach { rowItem ->
                    rowItem.columns = ((columns * rowItem.imageRatio) / rowRatios).toInt()
                    used += rowItem.columns
                }
                item.columns = columns - used
                row.clear()
                rowRatios = 0f
            } else {
                row.add(item)
            }

        }

        list.last().isLast = true

        return list
    }


}
